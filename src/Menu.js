import * as pixi from './PixiConstants.js';

export class Menu{

	constructor(stageDimensions){
		this._stage = new pixi.Container();
		
		this._stage.x = stageDimensions.x;
		this._stage.y = stageDimensions.y;

		const menuBackground = new pixi.Graphics();
		menuBackground.beginFill(0x000000);
		menuBackground.drawRect(0, 0, stageDimensions.width, stageDimensions.height);
		menuBackground.endFill();
		menuBackground.alpha = 0.65;
		menuBackground.x = 0;
		menuBackground.y = 0;

		this._stage.addChild(menuBackground);

		const txtStyle = new pixi.TextStyle({
			fontFamily: "ShowcardGothic",
			fontSize: 48,
			fill: "#FCBD00",
			stroke: 'black',
			strokeThickness: 2,
			letterSpacing: 3
		});

		const menuButtons = new pixi.Container();
		this._stage.addChild(menuButtons);

		//Button 1
		this._buttonChallenge1 = new pixi.Container();
		menuButtons.addChild(this._buttonChallenge1);

		const txtChallenge1 = new pixi.Text("Sprites",txtStyle);

		const buttonHeight = stageDimensions.height/6 - txtChallenge1.height/2;
		const buttonChallenge1Bg = new pixi.Graphics();
		buttonChallenge1Bg.lineStyle(3, 0x000000, 1);
		buttonChallenge1Bg.beginFill(0x7E00FC);
		buttonChallenge1Bg.drawRoundedRect(0, 0, stageDimensions.width/2, buttonHeight, 20);
		buttonChallenge1Bg.endFill();
		buttonChallenge1Bg.alpha = 0.6;

		txtChallenge1.position.set((buttonChallenge1Bg.width - txtChallenge1.width)/2,(buttonChallenge1Bg.height - txtChallenge1.height)/2);

		this._buttonChallenge1.addChild(buttonChallenge1Bg);
		this._buttonChallenge1.addChild(txtChallenge1);
		this._buttonChallenge1.x = (stageDimensions.width - buttonChallenge1Bg.width)/2;
		this._buttonChallenge1.y = 0;
		this._buttonChallenge1.interactive = true;
		this._buttonChallenge1.buttonMode = true;


		//Button 2
		this._buttonChallenge2 = new pixi.Container();
		menuButtons.addChild(this._buttonChallenge2);
		const txtChallenge2 = new pixi.Text("Text Tool",txtStyle);

		const buttonChallenge2Bg = buttonChallenge1Bg.clone();
		buttonChallenge2Bg.alpha = 0.6;

		txtChallenge2.position.set((buttonChallenge2Bg.width - txtChallenge2.width)/2,(buttonChallenge2Bg.height - txtChallenge2.height)/2);

		this._buttonChallenge2.addChild(buttonChallenge2Bg);
		this._buttonChallenge2.addChild(txtChallenge2);
		this._buttonChallenge2.x = (stageDimensions.width - buttonChallenge2Bg.width)/2;
		this._buttonChallenge2.y = stageDimensions.height/6 + this._buttonChallenge1.y;
		this._buttonChallenge2.interactive = true;
		this._buttonChallenge2.buttonMode = true;

		//Button 3
		this._buttonChallenge3 = new pixi.Container();
		menuButtons.addChild(this._buttonChallenge3);
		const txtChallenge3 = new pixi.Text("Particle",txtStyle);

		const buttonChallenge3Bg = buttonChallenge1Bg.clone();
		buttonChallenge3Bg.alpha = 0.6;

		txtChallenge3.position.set((buttonChallenge3Bg.width - txtChallenge3.width)/2,(buttonChallenge3Bg.height - txtChallenge3.height)/2);

		this._buttonChallenge3.addChild(buttonChallenge3Bg);
		this._buttonChallenge3.addChild(txtChallenge3);
		this._buttonChallenge3.x = (stageDimensions.width - buttonChallenge3Bg.width)/2;
		this._buttonChallenge3.y = stageDimensions.height/6 + this._buttonChallenge2.y;
		this._buttonChallenge3.interactive = true;
		this._buttonChallenge3.buttonMode = true;

		menuButtons.y = (this._stage.height - menuButtons.height)/2;
	}

	get stage(){
		return this._stage;
	}

	get buttonChallenge1(){
		return this._buttonChallenge1;
	}

	get buttonChallenge2(){
		return this._buttonChallenge2;
	}

	get buttonChallenge3(){
		return this._buttonChallenge3;
	}
}
