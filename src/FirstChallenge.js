import * as assets from './Assets.js';
import * as pixi from './PixiConstants.js';

export class FirstChallenge{

    constructor(){
        this._stage = new pixi.Container();
        this._txtFps;
        this._apples = [];
        this._index = 0;
        this._offsetX = 5+34;
        this._offsetY = 50;
        this._marginX = 5;
        this._marginY = 1;
    }

    prepare(stageDimensions){
        
        for (let i = 0; i < 144; i++) {
            const apple = new pixi.Sprite(pixi.Resources[assets.applePath].texture);
            apple.x = stageDimensions.x+stageDimensions.width - this._offsetX - this._marginX*i;
            apple.y = stageDimensions.y+this._offsetY + this._marginY*i;
            this._apples.push(apple);
            this._stage.addChild(apple);
        }

        this._apples.reverse();

        const txtStyle = new pixi.TextStyle({
            fontFamily: "ShowcardGothic",
            fontSize: 24,
            fill: "#FCBD00",
            stroke: 'black',
            strokeThickness: 2,
            letterSpacing: 3
        });

        this._txtFps = new pixi.Text("FPS: 60",txtStyle);
        this._stage.addChild(this._txtFps);
    }

    move(stageDimensions){

        if(this._index < this._apples.length){        
            const nextPosX = stageDimensions.x + stageDimensions.width - this._offsetX - this._marginX*this._index;
            const nextPosY = stageDimensions.y + stageDimensions.height - (this._offsetY+35) - this._marginY*this._index;

            this._stage.setChildIndex(this._apples[this._index],this._stage.children.length-1);

            TweenMax.to(this._apples[this._index], 2, {x: nextPosX, y: nextPosY, ease: Back.easeOut.config(1)});
            this._index++;
        }
    }

    clear(){
        this._index = 0;
        this._stage.visible = false;
        this._stage.removeChildren();
        this._apples = [];
    }

    get stage(){
        return this._stage;
    }

    set txtFps(value){
        this._txtFps.text = "FPS: "+value;
    }
}