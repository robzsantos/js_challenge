import * as assets from './Assets.js';
import * as pixi from './PixiConstants.js';

export class Game{

	constructor(){
		this._stage = new pixi.Container();

		this._background = new pixi.Sprite(pixi.Resources[assets.backgroundPath].texture);

		let scaleX = window.innerWidth/this._background.width;
		let scaleY = window.innerHeight/this._background.height;

		if(scaleX < scaleY) scaleY = scaleX;
		else scaleX = scaleY;

		this._background.scale.set(scaleX, scaleY);
		this._background.x = (window.innerWidth - this._background.width)/2;
		this._background.y = (window.innerHeight - this._background.height)/2;
		this._stage.addChild(this._background);

		this._rectangle = new pixi.Graphics();
		this._rectangle.beginFill(0x000000);
		this._rectangle.drawRect(0, 0, this._background.width, this._background.height);
		this._rectangle.antialias = true;
		this._rectangle.endFill();
		this._rectangle.alpha = 0.35;
		this._rectangle.x = this._background.x;
		this._rectangle.y = this._background.y;

		this._stage.addChild(this._rectangle);

		this._backButton = new pixi.Container();
		this._stage.addChild(this._backButton);
		this._backButton.visible = false;
	
		const txtBackButton = new pixi.Text("Menu",new pixi.TextStyle({
			fontFamily: "ShowcardGothic",
			fontSize: 24,
			fill: "#FCBD00",
			stroke: 'black',
			strokeThickness: 2,
			letterSpacing: 3
		}));

		const backButtonBg = new pixi.Graphics();
		backButtonBg.lineStyle(3, 0x000000, 1);
		backButtonBg.beginFill(0x7E00FC);
		backButtonBg.drawRoundedRect(0, 0, 250, 75, 20);
		backButtonBg.endFill();
		backButtonBg.alpha = 0.6;

		txtBackButton.position.set((backButtonBg.width - txtBackButton.width)/2,(backButtonBg.height - txtBackButton.height)/2);

		this._backButton.addChild(backButtonBg);
		this._backButton.addChild(txtBackButton);
		this._backButton.x = this._background.x+20;
		this._backButton.y = this._background.y+this._background.height - this._backButton.height - 20;
		this._backButton.interactive = true;
		this._backButton.buttonMode = true;
	}

	get stage(){
		return this._stage;
	}

	get background(){
		return this._background;
	}

	get backButton(){
		return this._backButton;
	}

}