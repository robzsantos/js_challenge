import * as assets from './Assets.js';
import * as pixi from './PixiConstants.js';
import { Game } from './Game.js';
import { Menu } from './Menu.js';
import { FirstChallenge } from './FirstChallenge.js';
import { SecondChallenge } from './SecondChallenge.js';
import { ThirdChallenge } from './ThirdChallenge.js';

const app = new pixi.Application();
app.renderer.view.style.position = "absolute";
app.renderer.view.style.display = "block";
app.renderer.autoResize = true;
app.renderer.antialias = true;
app.renderer.resize(window.innerWidth, window.innerHeight);

const root = document.getElementById('root');
root.appendChild(app.view);

window.addEventListener("resize", function(event){ 
  scaleToWindow(app.renderer.view);
});

pixi.Loader
  .add([assets.backgroundPath,
  	assets.applePath,
  	assets.iconsPath,
  	assets.particlePath,
  	assets.flamePath,
  	assets.secondChallengeDataPath])
/*  .on("progress", loadProgressHandler)*/
  .load(setup);

//TODO :: create a loading
/*function loadProgressHandler(loader, resource) {
	console.log("loading: " + resource.url); 
	console.log("progress: " + loader.progress + "%");
}*/

let state, gameScene, menuScene, firstChallengeScene, 
	secondChallengeScene, thirdChallengeScene,
	mLastElapsedTime = -1;

function setup(){
	state = menu;

	gameScene = new Game();
	app.stage.addChild(gameScene.stage);

	menuScene = new Menu(gameScene.background);
	app.stage.addChild(menuScene.stage);
	menuScene.stage.visible = false;

	firstChallengeScene = new FirstChallenge();
	app.stage.addChild(firstChallengeScene.stage);
	firstChallengeScene.stage.visible = false;

	secondChallengeScene = new SecondChallenge();
	app.stage.addChild(secondChallengeScene.stage);
	secondChallengeScene.stage.visible = false;

	thirdChallengeScene = new ThirdChallenge();
	app.stage.addChild(thirdChallengeScene.stage);
	thirdChallengeScene.stage.visible = false;

	initListeners();
	secondChallengeScene.prepare(gameScene.background);
	thirdChallengeScene.prepare(gameScene.background);

	app.ticker.add(delta => gameLoop(delta));
}

function initListeners(){

	menuScene.buttonChallenge1.on('pointertap', () => {
			firstChallengeScene.prepare(gameScene.background);
			state = challenge1;
		});

	menuScene.buttonChallenge2.on('pointertap', () => {
			state = challenge2;
		});

	menuScene.buttonChallenge3.on('pointertap', () => {
			thirdChallengeScene.handleParticle = true;
			state = challenge3;
		});
	gameScene.backButton.on('pointertap', () => {
			mLastElapsedTime = -1;
			firstChallengeScene.clear();
			secondChallengeScene.stage.visible = false;
			thirdChallengeScene.stage.visible = false;
			thirdChallengeScene.handleParticle = false;
			gameScene.backButton.visible = false;
			state = menu;
		});
}

function gameLoop(delta){

	state(delta);
	app.render(app.view);
}

function menu(delta){

	menuScene.stage.visible = true;
}


function challenge1(delta){

	menuScene.stage.visible = false;
	gameScene.backButton.visible = true;
	firstChallengeScene.stage.visible = true;

	const currentTime = Math.round(app.ticker.lastTime/1000);

	if(currentTime != mLastElapsedTime){
		mLastElapsedTime = currentTime;
		firstChallengeScene.move(gameScene.background);
	}

	firstChallengeScene.txtFps = app.ticker.FPS.toFixed(2);
}

function challenge2(){

	menuScene.stage.visible = false;
	gameScene.backButton.visible = true;
	secondChallengeScene.stage.visible = true;

	const currentTime = Math.round(app.ticker.lastTime/1000);

	if(currentTime != mLastElapsedTime && currentTime%2 == 0){
		mLastElapsedTime = currentTime;
		secondChallengeScene.createText();
	}

	secondChallengeScene.txtFps = app.ticker.FPS.toFixed(2);

}

let elapsed = Date.now();

function challenge3(){

	menuScene.stage.visible = false;
	gameScene.backButton.visible = true;
	thirdChallengeScene.stage.visible = true;

	const now = Date.now();
	thirdChallengeScene.update(now-elapsed);
	elapsed = now;

	thirdChallengeScene.txtFps = app.ticker.FPS.toFixed(2);
}