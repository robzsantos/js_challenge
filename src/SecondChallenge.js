import * as assets from './Assets.js';
import * as pixi from './PixiConstants.js';

export class SecondChallenge{

    constructor(){   
        this._stage = new pixi.Container();
        this._words = pixi.Resources[assets.secondChallengeDataPath].data.texts;
        this._icons = [];
        this._content = new pixi.Container();
        this._textStyle = new pixi.TextStyle({
            fontFamily: "ShowcardGothic",
            fontSize: 24,
            fill: "#FCBD00",
            stroke: 'black',
            strokeThickness: 2,
            letterSpacing: 3
        });
        this._txtFps = new pixi.Text("FPS: 60",this._textStyle);
        this._random = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min;
        this._clearContent = () => {
            this._content.x = 0;
            this._content.y = 0;
            this._content.removeChildren();
        }
        this._minFontSize = 24;
        this._maxFontSize = 70;
        this._numWords = 3;
    }

    prepare(stageDimensions){

        this._stage.x = stageDimensions.x;
        this._stage.y = stageDimensions.y;
        this._stage.width = stageDimensions.width;
        this._stage.height = stageDimensions.height;

        const spritesData = pixi.Resources[assets.secondChallengeDataPath].data.sprites;

        const rows = spritesData.rows;
        const columns = spritesData.columns;
        const emoticonWidth = spritesData.width;
        const emoticonHeight = spritesData.height;
        
        const iconsTexture = pixi.Resources[assets.iconsPath].texture;
        
        for (let i = 0; i < rows; i++) {
            for (let j = 0; j < columns; j++) {
                const frame = new pixi.Rectangle(emoticonWidth*j,emoticonHeight*i,emoticonWidth,emoticonHeight);
                const emoticon = new pixi.Sprite(new pixi.Texture(iconsTexture.baseTexture, frame));
                emoticon.name = ""+i+j;
                this._icons.push(emoticon);
            }
        }

        const textContainer = new pixi.Container();
        this._stage.addChild(textContainer);

        const textBackground = new pixi.Graphics();
        textBackground.beginFill(0x7E00FC);
        textBackground.drawRect(0, 0, this._stage._width - 150, this._stage._height/6);
        textBackground.endFill();
        textBackground.alpha = 0.6;

        textContainer.x = (this._stage._width - textBackground.width)/2;
        textContainer.y = (this._stage._height - textBackground.height)/2;

        textContainer.addChild(textBackground);
        textContainer.addChild(this._content);

        this._stage.addChild(this._txtFps);
    }

    createText(){
        let offsetX = 0;
        const marginRight = 10; 

        if(this._content.children.length > 0){
            this._clearContent();
        }

        for (let i = 0; i < this._numWords; i++) {
            const type = this._random(0,1);

            if(type == 0){
                const text = this._words[this._random(0,this._words.length-1)];
                const fontSize = this._random(this._minFontSize,this._maxFontSize);
                const style = this._textStyle.clone();
                style.fontSize = fontSize;
                const word = new pixi.Text(text,style);
                word.x = offsetX;
                offsetX = word.x+word.width+marginRight;

                this._content.addChild(word);
            }
            else { 
                const icon = this._icons[this._random(0,this._icons.length-1)];
                const sprite = pixi.Sprite.from(icon.texture);
                sprite.x = offsetX;
                offsetX = sprite.x+sprite.width+marginRight;

                this._content.addChild(sprite);
            }
        }

        this._content.children.forEach( (elem) =>{
                elem.y = (this._content.height - elem.height)/2;
            }
        );

        this._content.x = (this._content.parent.width - this._content.width)/2;
        this._content.y = (this._content.parent.height - this._content.height)/2;
    }

    get stage(){
        return this._stage;
    }

    set txtFps(value){
        this._txtFps.text = "FPS: "+value;
    }

}