import * as assets from './Assets.js';
import * as pixi from './PixiConstants.js';

export class ThirdChallenge{

    constructor(){
        this._stage = new pixi.Container();
        this._emitter;
        this._txtFps;
    }

    prepare(stageDimensions){

        this._stage.x = stageDimensions.x;
        this._stage.y = stageDimensions.y;
        this._stage.width = stageDimensions.width;
        this._stage.height = stageDimensions.height;

        const config = {
            "alpha": {
                "start": 1,
                "end": 0.7
            },
            "scale": {
                "start": 0.25,
                "end": 0.5,
                "minimumScaleMultiplier": 1
            },
            "color": {
                "list": [
                    {"value":"fff191", "time":0},
                    {"value":"ff622c", "time":1}
                ],
                "isStepped": false
            },
            "speed": {
                "start": 200,
                "end": 200,
                "minimumSpeedMultiplier": 1
            },
            "acceleration": {
                "x": 0,
                "y": 0
            },
            "maxSpeed": 0,
            "startRotation": {
                "min": 265,
                "max": 275
            },
            "noRotation": false,
            "rotationSpeed": {
                "min": 50,
                "max": 50
            },
            "lifetime": {
                "min": 0.1,
                "max": 0.5
            },
            "blendMode": "normal",
            "frequency": 0.04,
            "emitterLifetime": -1,
            "maxParticles": 10,
            "pos": {
                "x": 0,
                "y": 0
            },
            "addAtBack": false,
            "spawnType": "circle",
            "spawnCircle": {
                "x": 0,
                "y": 0,
                "r": 10
            }
        };

        const stepColors = 5;

        const txtStyle = new pixi.TextStyle({
            fontFamily: "ShowcardGothic",
            fontSize: 24,
            fill: "#FCBD00",
            stroke: 'black',
            strokeThickness: 2,
            letterSpacing: 3
        });

        this._txtFps = new pixi.Text("FPS: 60",txtStyle);
        this._stage.addChild(this._txtFps);

        let art;
        art = [];
        art.push(pixi.Texture.fromImage(assets.flamePath));
        art.push(pixi.Texture.fromImage(assets.particlePath));

        const emitterContainer = new pixi.Container();
        this._stage.addChild(emitterContainer);
        this._emitter = new pixi.Particles.Emitter(
            emitterContainer,
            art,
            config
        );
        if (stepColors)
            this._emitter.startColor = pixi.Particles.ParticleUtils.createSteppedGradient(config.color.list, stepColors);

        this._emitter.updateOwnerPos(this._stage._width / 2, this._stage._height / 2);
    }

    update(currentTime){
        if (this._emitter)
            this._emitter.update(currentTime*0.001);
    }

    handleParticle(value){
        emitter.emit = value;
    }

    destroy(){
        this._emitter.destroy();
        this._emitter = null;

        //reset SpriteRenderer's batching to fully release particles for GC
        if (renderer.plugins && renderer.plugins.sprite && renderer.plugins.sprite.sprites)
            renderer.plugins.sprite.sprites.length = 0;

        renderer.render(stage);
    }

    get stage(){
        return this._stage;
    }

    set txtFps(value){
        this._txtFps.text = "FPS: "+value;
    }

}