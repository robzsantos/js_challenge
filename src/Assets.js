export const secondChallengeDataPath = "./assets/second_challenge.json";
export const backgroundPath = "./assets/images/background.png";
export const applePath = "./assets/images/apple.png";
export const iconsPath = "./assets/images/icons.png";
export const particlePath = "./assets/images/particle.png";
export const flamePath = "./assets/images/flame.png";